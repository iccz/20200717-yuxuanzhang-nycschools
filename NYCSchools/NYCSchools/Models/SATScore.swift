//
//  SATScore.swift
//  NYCSchools
//
//  Created by Isaac Zhang on 7/17/20.
//  Copyright © 2020 Yu Xuan Zhang. All rights reserved.
//

import Foundation

struct SATScore: Decodable {
    // reason why these props are string is that some value can be "s"
    let numTestTakers: String
    let readingAvg: String
    let mathAvg: String
    let writingAvg: String
    
    enum CodingKeys: String, CodingKey {
        case numTestTakers = "num_of_sat_test_takers"
        case readingAvg = "sat_critical_reading_avg_score"
        case mathAvg = "sat_math_avg_score"
        case writingAvg = "sat_writing_avg_score"
    }
}
