//
//  School.swift
//  NYCSchools
//
//  Created by Isaac Zhang on 7/17/20.
//  Copyright © 2020 Yu Xuan Zhang. All rights reserved.
//

import Foundation

struct School: Decodable {
    let dbn: String
    let name: String
    let overview: String
    let location: String
    let phone: String
    let website: String
    let totalStudents: Int
    let city: String
    let zip: String
    let state: String
    var score: SATScore? = nil // fetch from a seperate api call, so default set to nil
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        dbn = try values.decode(String.self, forKey: .dbn)
        name = try  values.decode(String.self, forKey: .name)
        overview = try values.decode(String.self, forKey: .overview)
        location = try values.decode(String.self, forKey: .location)
        phone = try values.decode(String.self, forKey: .phone)
        website = try values.decode(String.self, forKey: .website)
        guard let totalStudents = try Int(values.decode(String.self, forKey: .totalStudents)) else {
            throw DecodingError.dataCorrupted(DecodingError.Context(codingPath: [CodingKeys.totalStudents], debugDescription: "Expecting string format of Int"))
        }
        self.totalStudents = totalStudents
        city = try values.decode(String.self, forKey: .city)
        zip = try values.decode(String.self, forKey: .zip)
        state = try values.decode(String.self, forKey: .state)
    }
    
    enum CodingKeys: String, CodingKey {
        case dbn, website, city, zip
        case name = "school_name"
        case overview = "overview_paragraph"
        case totalStudents = "total_students"
        case phone = "phone_number"
        case location = "primary_address_line_1"
        case state = "state_code"
    }
    
    func area() -> String {
        return "\(city), \(state) \(zip)"
    }
    
    func fullAddress() -> String {
        return "\(location), \(area())"
    }
}
