//
//  Constants.swift
//  NYCSchools
//
//  Created by Isaac Zhang on 7/17/20.
//  Copyright © 2020 Yu Xuan Zhang. All rights reserved.
//

import Foundation

struct SegueIdentifier {
    static let listToDetail = "listToDetail"
}

enum APIError: Error {
    case noDetailFound
}
