//
//  URLRequest+Misc.swift
//  NYCSchools
//
//  Created by Isaac Zhang on 7/17/20.
//  Copyright © 2020 Yu Xuan Zhang. All rights reserved.
//

import Foundation

fileprivate enum HttpMethod: String {
    case get, post, put, delete
}
extension URLRequest {
    static func requestWithToken(_ urlString: String) ->URLRequest? {
        guard let url = URL(string: urlString) else {
            return nil
        }
        
        var request = URLRequest(url: url)
        // add api token
        request.addValue("k7kyMj9TX0wyb56CgjdFxGHet", forHTTPHeaderField: "X-App-Token")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = HttpMethod.get.rawValue
        return request
    }
}
