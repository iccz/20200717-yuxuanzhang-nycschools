//
//  SATScoreFetchOperation.swift
//  NYCSchools
//
//  Created by Isaac Zhang on 7/17/20.
//  Copyright © 2020 Yu Xuan Zhang. All rights reserved.
//

import UIKit

class SATScoreFetchOperation: AsyncOperation<SATScore> {
    private let base = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
    private let session: URLSession
    private let dbn: String // school dbn
    
    init(_ dbn: String, _ session: URLSession = URLSession.shared) {
        self.session = session
        self.dbn = dbn
    }
    
    override func main() {
        guard let request = URLRequest.requestWithToken("\(base)\(dbn)") else {
            finish()
            return
        }
        
        session.dataTask(with: request) { (data, response, error) in
            guard let response = response, let data = data else {
                DispatchQueue.main.async {
                    self.complete(result: .failure(error ?? URLError(.unknown)))
                }
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                DispatchQueue.main.async {
                    self.complete(result: .failure(error ?? URLError(.cannotParseResponse)))
                }
                return
            }
            
            guard 200 ..< 300 ~= httpResponse.statusCode else {
                DispatchQueue.main.async {
                    self.complete(result: .failure(error ?? URLError(.badServerResponse)))
                }
                return
            }
            
            do {
                // api return a list even after filter, so we have to parse with list
                let scoresList = try JSONDecoder().decode([SATScore].self, from: data)
                
                // we should only have one result
                guard scoresList.count == 1 else {
                    DispatchQueue.main.async {
                        self.complete(result: .failure(APIError.noDetailFound))
                    }
                    return
                }
                
                DispatchQueue.main.async {
                    self.complete(result: .success(scoresList[0]))
                }
            }
                
            catch let error {
                DispatchQueue.main.async {
                    self.complete(result: .failure(error))
                }
                return
            }
        }.resume()
    }
}
