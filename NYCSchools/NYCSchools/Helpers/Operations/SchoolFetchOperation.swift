//
//  SchoolFetchOperation.swift
//  NYCSchools
//
//  Created by Isaac Zhang on 7/17/20.
//  Copyright © 2020 Yu Xuan Zhang. All rights reserved.
//

import Foundation

class SchoolFetchOperation: AsyncOperation<[School]> {
    private let base = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$limit=%d&$offset=%d"
    
    private let session: URLSession
    private let offset: Int // offset to fetch school
    private let limit: Int // limit number of the fetch results
    
    
    init(_ offset: Int, _ limit: Int, _ session: URLSession = URLSession.shared) {
        self.session = session
        self.offset = offset
        self.limit = limit
    }
    
    override func main() {
        guard let request = URLRequest.requestWithToken(String(format: base, limit, offset)) else {
            finish()
            return
        }
        
        session.dataTask(with: request) { (data, response, error) in
            guard let response = response, let data = data else {
                DispatchQueue.main.async {
                    self.complete(result: .failure(error ?? URLError(.unknown)))
                }
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                DispatchQueue.main.async {
                    self.complete(result: .failure(error ?? URLError(.cannotParseResponse)))
                }
                return
            }
            
            guard 200 ..< 300 ~= httpResponse.statusCode else {
                DispatchQueue.main.async {
                    self.complete(result: .failure(error ?? URLError(.badServerResponse)))
                }
                return
            }
            
            do {
                let schools = try JSONDecoder().decode([School].self, from: data)
                DispatchQueue.main.async {
                    self.complete(result: .success(schools))
                }
            }
                
            catch let error {
                DispatchQueue.main.async {
                    self.complete(result: .failure(error))
                }
                return
            }
        }.resume()
    }
}
