//
//  AsyncOperation.swift
//  NYCSchools
//
//  Created by Isaac Zhang on 7/17/20.
//  Copyright © 2020 Yu Xuan Zhang. All rights reserved.
//

import Foundation

enum FetchResult<T> {
    case success(T)
    case failure(Error)
}

// A helper async operation class I found on: https://gist.github.com/eappel/48093ad2fb826e98dc30d84befd573be
class AsyncOperation<T>: Operation {
    typealias OperationCompletionHandler = (_ result: FetchResult<T>) -> Void
    var completionHandler: OperationCompletionHandler?
    // MARK: - Internal State
    
    private var _isExecuting: Bool = false
    private var _isFinished: Bool = false
    
    // MARK: - Operation Overrides
    open override func start() {
        isExecuting = true
        main()
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    override var isExecuting: Bool {
        get {
            return _isExecuting
        }
        set {
            let key = "isExecuting"
            willChangeValue(forKey: key)
            _isExecuting = newValue
            didChangeValue(forKey: key)
        }
    }
    
    override var isFinished: Bool {
        get {
            return _isFinished
        }
        set {
            let key = "isFinished"
            willChangeValue(forKey: key)
            _isFinished = newValue
            didChangeValue(forKey: key)
        }
    }
    
    // MARK: - Helper
    
    open func finish() {
        isExecuting = false
        isFinished = true
    }
    
    func complete(result: FetchResult<T>) {
        finish()
        
        if !isCancelled {
            completionHandler?(result)
        }
    }
}
