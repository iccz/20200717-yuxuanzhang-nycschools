//
//  SchoolsTableViewController.swift
//  NYCSchools
//
//  Created by Isaac Zhang on 7/17/20.
//  Copyright © 2020 Yu Xuan Zhang. All rights reserved.
//

import UIKit

class SchoolsTableViewController: UITableViewController {
    var schoolManager = SchoolManager(QueueManager())
    private lazy var spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .large)
        spinner.frame = CGRect(x: 0, y: 0, width: self.tableView.bounds.width, height: 44)
        return spinner
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Schools"
        tableView.register(UINib(nibName: SchoolTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: SchoolTableViewCell.identifier)
        
        fetchMoreSchools()
    }
    
    private func fetchMoreSchools() {
        // check if we have fetched all schools
        guard schoolManager.fetchedFullList == false else {
            return
        }
        
        showSpinner(doShow: true)
        let oldCount = schoolManager.schools.count
        schoolManager.fetchSchool(schoolManager.schools.count) { (result, fetchedFullList) in
            switch result {
            case .success(let newSchools):
                // get new indexPaths so we do not need to reload full list
                let indexPaths = newSchools.enumerated().map { (index, _) -> IndexPath in
                    return IndexPath(row: oldCount + index, section: 0)
                }
                
                self.tableView.insertRows(at: indexPaths, with: .bottom)
                self.showSpinner(doShow: false)

            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    private func showSpinner(doShow: Bool) {
        if doShow {
            tableView.tableFooterView = spinner
            spinner.startAnimating()
        }
        else {
            self.tableView.tableFooterView = nil
            spinner.stopAnimating()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case SegueIdentifier.listToDetail:
            let vc = segue.destination as! SchoolDetailViewController
            vc.school = sender as? School
            vc.schoolManager = schoolManager
        default:
            break
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolManager.schools.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SchoolTableViewCell.identifier, for: indexPath) as! SchoolTableViewCell
        let school = schoolManager.schools[indexPath.row]
        cell.setup(school)
        
        //  if we are at the last 5 elements, we fetch more if there is any
        if indexPath.row == schoolManager.schools.count - 5 {
            fetchMoreSchools()
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let school = schoolManager.schools[indexPath.row]
        performSegue(withIdentifier: SegueIdentifier.listToDetail, sender: school)
    }
}
