//
//  SchoolDetailViewController.swift
//  NYCSchools
//
//  Created by Isaac Zhang on 7/17/20.
//  Copyright © 2020 Yu Xuan Zhang. All rights reserved.
//

import UIKit

class SchoolDetailViewController: UIViewController {
    var school: School?
    var schoolManager: SchoolManager?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var totalStudentLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var scoreLoadingSpinner: UIActivityIndicatorView!
    @IBOutlet weak var noScoreFoundLabel: UILabel!
    @IBOutlet weak var numOfTakersLabel: UILabel!
    @IBOutlet weak var readingAvgLabel: UILabel!
    @IBOutlet weak var mathAvgLabel: UILabel!
    @IBOutlet weak var writingAvgLabel: UILabel!
    @IBOutlet weak var scoreStackView: UIStackView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Detail"
        
        updateView()
    }
    
    private func updateView() {
        guard let school = school, let schoolManager = schoolManager else {
            return
        }
        
        showSchoolInfo()

        // we do not know if we have score yet, only show when we confirmed
        noScoreFoundLabel.isHidden = true
        scoreStackView.isHidden = true
        
        if school.score != nil {
            showScoreInfo()
        }
        else {
            scoreLoadingSpinner.startAnimating()
            schoolManager.getDetail(school.dbn, { (result) in
                self.scoreLoadingSpinner.stopAnimating()
                switch result {
                case .success(let newScore):
                    self.school?.score = newScore
                default:
                    break
                }
                
                self.showScoreInfo()
            })
        }
    }
    
    // basic school info
    private func showSchoolInfo() {
        guard let school = school else {
            return
        }
        
        nameLabel.text = school.name
        overviewLabel.text = school.overview
        totalStudentLabel.text = "Students Count: \(school.totalStudents)"
        locationLabel.text = school.fullAddress()
        
        //TODO: if given more time, I would implement a webKitView to show the school website and make the phone number to be able to call directly
        phoneLabel.text = school.phone
        websiteLabel.text = school.website
    }
    
    private func showScoreInfo() {
        guard let school = school, let score = school.score else {
            scoreStackView.isHidden = true
            noScoreFoundLabel.isHidden = false
            return
        }
        
        scoreStackView.isHidden = false
        noScoreFoundLabel.isHidden = true

        numOfTakersLabel.text = "Test Takers: \(score.numTestTakers)"
        readingAvgLabel.text = "Reading Average: \(score.readingAvg)"
        mathAvgLabel.text = "Math Average: \(score.mathAvg)"
        writingAvgLabel.text = "Writing Average: \(score.writingAvg)"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
