//
//  QueueManager.swift
//  NYCSchools
//
//  Created by Isaac Zhang on 7/17/20.
//  Copyright © 2020 Yu Xuan Zhang. All rights reserved.
//

import Foundation

struct QueueManager {
    let queue = OperationQueue()
    
    func add(_ operation: Operation) {
        queue.addOperation(operation)
    }
}
