//
//  SchoolManager.swift
//  NYCSchools
//
//  Created by Isaac Zhang on 7/17/20.
//  Copyright © 2020 Yu Xuan Zhang. All rights reserved.
//

import Foundation

typealias SchoolFetchBlock = (_ result: FetchResult<[School]>, _ fetchedFullList: Bool) -> Void
typealias DetailFetchBlock = (_ result: FetchResult<SATScore>) -> Void

// using class cause we need to change schools property
class SchoolManager {
    private let queueManager: QueueManager
    private let session: URLSession
    private let fetchLimit = 20 // we fetch 20 schools per request

    var schools = [School]()
    var fetchedFullList = false

    init(_ queueManager: QueueManager, session: URLSession = URLSession.shared) {
        self.queueManager = queueManager
        self.session = session
    }
    
    func fetchSchool(_ index: Int, _ block: SchoolFetchBlock?) {
        let operation = SchoolFetchOperation(index, fetchLimit, session)
        operation.completionHandler = { result in
            switch result {
            case .success(let newSchools):
                self.schools.append(contentsOf: newSchools)
                // if we fetched lest than limit, it means we have fetched all data
                block?(result, newSchools.count < self.fetchLimit)
            case .failure(_):
                block?(result, false)
            }
        }
        queueManager.add(operation)
    }
    
    func getDetail(_ schoolDbn: String, _ block: DetailFetchBlock?) {
        let operation = SATScoreFetchOperation(schoolDbn, session)
        operation.completionHandler = { result in
            switch result {
            case .success(let newScores):
                guard let index = self.schools.firstIndex(where: { school in
                    return school.dbn == schoolDbn
                }) else {
                    block?(.failure(APIError.noDetailFound))
                    return
                }
                
                self.schools[index].score = newScores
                block?(.success(newScores))
                
            case .failure(let error):
                block?(.failure(error))
            }
        }
        queueManager.add(operation)
    }
}
