//
//  SchoolTableViewCell.swift
//  NYCSchools
//
//  Created by Isaac Zhang on 7/17/20.
//  Copyright © 2020 Yu Xuan Zhang. All rights reserved.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    static let identifier = "SchoolTableViewCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cityStateZipLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(_ school: School) {
        nameLabel.text = school.name
        cityStateZipLabel.text = school.area()
    }
}
