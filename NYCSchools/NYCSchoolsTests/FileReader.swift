//
//  FileReader.swift
//  NYCSchools
//
//  Created by Isaac Zhang on 7/17/20.
//  Copyright © 2020 Yu Xuan Zhang. All rights reserved.
//

import Foundation

import Foundation

class FileReader {
    static func read(_ fileName: String) -> Data? {
        guard let path = Bundle(for: FileReader.self).path(forResource: fileName, ofType: "json") else {
            return nil
        }
        
        do {
            let content = try String(contentsOfFile: path)
            
            guard let data = content.data(using: .utf8) else {
                return nil
            }
            
            return data
        }
        catch {
            return nil
        }
    }
}
