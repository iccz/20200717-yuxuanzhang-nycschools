//
//  SchoolTests.swift
//  NYCSchoolsTests
//
//  Created by Isaac Zhang on 7/17/20.
//  Copyright © 2020 Yu Xuan Zhang. All rights reserved.
//

import XCTest
@testable import NYCSchools

fileprivate enum FileNames: String {
    case schoolArray = "school_array"
    case schoolSingle = "school_single"
    case scoreSingle = "sat_score_single"
    case scoreArray = "sat_score_array"
}

class ModelTests: XCTestCase {
    let decoder = JSONDecoder()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSchoolArrayConversion() throws {
        let data = FileReader.read(FileNames.schoolArray.rawValue)
        XCTAssertNotNil(data, "can not read from file")
                
        let schools = try decoder.decode([School].self, from: data!)
        XCTAssertTrue(schools.count != 0, "schools array should have more than 0")
    }
    
    func testSchoolSingleConversion() throws {
        let data = FileReader.read(FileNames.schoolSingle.rawValue)
        XCTAssertNotNil(data, "can not read from file")
                
        let school = try decoder.decode(School.self, from: data!)
        
        XCTAssertTrue(school.totalStudents != 0, "total student should not be 0")
        XCTAssertNil(school.score, "score should be nil")
    }
    
    func testScoreArrayConversion() throws {
        let data = FileReader.read(FileNames.scoreArray.rawValue)
        XCTAssertNotNil(data, "can not read from file")
                
        let scoresArray = try decoder.decode([SATScore].self, from: data!)
        XCTAssertTrue(scoresArray.count != 0, "score array should have more than 0")
    }
    
    func testScoreSingleConversion() throws {
        let data = FileReader.read(FileNames.scoreSingle.rawValue)
        XCTAssertNotNil(data, "can not read from file")
                
        _ = try decoder.decode(SATScore.self, from: data!)
    }
}

